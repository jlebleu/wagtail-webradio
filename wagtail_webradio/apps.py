from django.apps import AppConfig


class WagtailWebRadioAppConfig(AppConfig):
    name = 'wagtail_webradio'
    verbose_name = 'Wagtail Web Radio'
    default_auto_field = "django.db.models.AutoField"
