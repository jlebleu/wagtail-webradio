import os

from django.conf import settings
from django.contrib.auth.models import Group, Permission
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from django.utils.translation import gettext
from django.utils.translation import gettext_lazy as _

from wagtail.core.fields import RichTextField
from wagtail.search import index

import magic
from modelcluster.contrib.taggit import ClusterTaggableManager
from modelcluster.fields import ParentalKey
from modelcluster.models import ClusterableModel
from taggit.models import TaggedItemBase

from .utils import format_duration

DESCRIPTION_EDITOR_FEATURES = ['bold', 'italic', 'link', 'ol', 'ul']

SOUND_PATH = 'podcasts'
SOUND_PATH_BY_RADIOSHOW = getattr(
    settings,
    'WEBRADIO_SOUND_PATH_BY_RADIOSHOW',
    False,
)

AUTHORIZED_MIME_TYPES = getattr(
    settings,
    'WEBRADIO_AUTHORIZED_MIME_TYPES',
    ['audio/ogg', 'audio/mpeg', 'audio/flac', 'audio/opus'],
)

# NB. magic failed if size is too short
MAGIC_READ_SIZE = 5 * (1024 * 1024)  # 5M


def upload_podcast_sound_to(instance, filename):
    """create the soundfile path of a podcast"""
    extension = os.path.splitext(filename)[1]
    if SOUND_PATH_BY_RADIOSHOW:
        radioshow = instance.radio_show.slug
        return f'{SOUND_PATH}/{radioshow}/{instance.slug}{extension}'
    else:
        return f'{SOUND_PATH}/{instance.slug}{extension}'


class AutoSlugMixin:
    """
    Generate a unique slug from another field - `title` by default - at
    validation as needed.

    The model which inherit this mixin must define the fields `SLUG_FIELD` and
    `TITLE_FIELD`.
    """

    SLUG_FIELD = 'slug'
    TITLE_FIELD = 'title'

    @classmethod
    def _slug_is_available(cls, slug, obj):
        queryset = cls.objects

        if obj.pk is not None:
            queryset = queryset.exclude(pk=obj.pk)

        return not queryset.filter(**{cls.SLUG_FIELD: slug}).exists()

    def _get_autogenerated_slug(self, base_slug):
        candidate_slug = base_slug
        suffix = 0

        while not self._slug_is_available(candidate_slug, self):
            suffix += 1
            candidate_slug = "%s-%d" % (base_slug, suffix)

        return candidate_slug

    def full_clean(self, *args, **kwargs):
        if not self.slug:
            base_slug = slugify(getattr(self, self.TITLE_FIELD))

            if base_slug:
                self.slug = self._get_autogenerated_slug(base_slug)

        super().full_clean(*args, **kwargs)

    def clean(self):
        super().clean()

        if not self._slug_is_available(getattr(self, self.SLUG_FIELD), self):
            raise ValidationError(
                {self.SLUG_FIELD: gettext("This slug is already in use.")}
            )


class RadioShow(AutoSlugMixin, index.Indexed, models.Model):
    title = models.CharField(verbose_name=_("title"), max_length=255)

    description = RichTextField(
        verbose_name=_("description"),
        blank=True,
        features=DESCRIPTION_EDITOR_FEATURES,
    )
    picture = models.ForeignKey(
        'wagtailimages.Image',
        verbose_name=_("picture"),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='+',
    )

    contact_phone = models.CharField(
        verbose_name=_("phone number"),
        blank=True,
        max_length=20,
    )
    contact_email = models.EmailField(verbose_name=_("email"), blank=True)

    slug = models.SlugField(
        verbose_name=_("slug"),
        max_length=255,
        unique=True,
    )

    search_fields = [
        index.SearchField('title', partial_match=True, boost=2),
        index.SearchField('description', partial_match=True),
    ]

    class Meta:
        verbose_name = _("radio show")
        verbose_name_plural = _("radio shows")

    def __str__(self):
        return self.title


class TaggedPodcast(TaggedItemBase):
    content_object = ParentalKey(
        'Podcast',
        on_delete=models.CASCADE,
        related_name='podcast_tags',
    )

    @classmethod
    def tags_for(cls, instance=None, **extra_filters):
        # We provide the 'model' argument as django-taggit could require it
        return super().tags_for(Podcast, instance, **extra_filters)


class PodcastManager(models.Manager):
    def currents(self):
        return self.filter(publish_date__lte=timezone.now())


class Podcast(AutoSlugMixin, index.Indexed, ClusterableModel):
    title = models.CharField(verbose_name=_("title"), max_length=255)

    description = RichTextField(
        verbose_name=_("description"),
        blank=True,
        features=DESCRIPTION_EDITOR_FEATURES,
    )
    picture = models.ForeignKey(
        'wagtailimages.Image',
        verbose_name=_("picture"),
        blank=True,
        null=True,
        on_delete=models.SET_NULL,
        related_name='+',
    )
    tags = ClusterTaggableManager(
        through=TaggedPodcast,
        verbose_name=_("tags"),
        blank=True,
    )

    sound_file = models.FileField(
        upload_to=upload_podcast_sound_to,
        verbose_name=_("sound file"),
        null=True,
        blank=True,
    )
    sound_url = models.URLField(
        verbose_name=_("sound URL"),
        blank=True,
    )
    duration = models.DurationField(
        verbose_name=_("duration"),
        blank=True,
        null=True,
    )

    radio_show = models.ForeignKey(
        'RadioShow',
        verbose_name=_("radio show"),
        on_delete=models.PROTECT,
        related_name='podcasts',
        related_query_name='podcast',
    )

    publish_date = models.DateTimeField(
        verbose_name=_("publish date"),
        default=timezone.now,
    )
    slug = models.SlugField(
        verbose_name=_("slug"),
        max_length=255,
        unique=True,
    )

    search_fields = [
        index.SearchField('title', partial_match=True, boost=2),
        index.SearchField('description', partial_match=True),
        index.FilterField('publish_date'),
        index.FilterField('radio_show_id'),
        index.RelatedFields('radio_show', [index.SearchField('title')]),
    ]

    objects = PodcastManager()

    class Meta:
        ordering = ['-publish_date']
        verbose_name = _("podcast")
        verbose_name_plural = _("podcasts")

    def clean_fields(self, exclude=None):
        super().clean_fields(exclude=exclude)
        if self.sound_file:
            content = self.sound_file.file.read(MAGIC_READ_SIZE)
            mime = magic.from_buffer(content, mime=True)
            if mime not in AUTHORIZED_MIME_TYPES:
                raise ValidationError(
                    {
                        'sound_file': [
                            _(
                                "The file type {} is not authorized. Authorized"
                                " file types are {}.".format(
                                    mime, ', '.join(AUTHORIZED_MIME_TYPES)
                                )
                            ),
                        ]
                    }
                )

    def clean(self):
        super().clean()

        if not self.sound_file and not self.sound_url:
            raise ValidationError(
                _("You must fill at least a sound file or a sound url"),
                code='at-least-one',
            )
        if self.sound_file and self.sound_url:
            raise ValidationError(
                _("Only one sound must be set (file or url)"),
                code='only-one-sound',
            )

    def __str__(self):
        return self.title

    def get_duration_display(self):
        return format_duration(self.duration)

    def get_picture(self):
        return self.picture or self.radio_show.picture

    @property
    def url(self):
        return self.sound_file.url if self.sound_file else self.sound_url


class GroupRadioShowPermissionManager(models.Manager):
    def get_by_natural_key(self, group, radio_show, permission):
        return self.get(
            group=group,
            radio_show=radio_show,
            permission=permission,
        )


class GroupRadioShowPermission(models.Model):
    """
    A rule indicating that a group has permission for some action - e.g. add a
    podcast - within a specified radio show.
    """

    group = models.ForeignKey(
        Group,
        verbose_name=_("group"),
        related_name='radio_show_permissions',
        on_delete=models.CASCADE,
    )
    radio_show = models.ForeignKey(
        RadioShow,
        verbose_name=_("radio show"),
        related_name='group_permissions',
        on_delete=models.CASCADE,
    )
    permission = models.ForeignKey(
        Permission,
        verbose_name=_("permission"),
        on_delete=models.CASCADE,
    )

    objects = GroupRadioShowPermissionManager()

    class Meta:
        unique_together = ('group', 'radio_show', 'permission')
        verbose_name = _("group radio show permission")
        verbose_name_plural = _("group radio show permissions")

    def __str__(self):
        return "Group %d ('%s') has permission '%s' on radio show %d ('%s')" % (
            self.group.id,
            self.group,
            self.permission,
            self.radio_show.id,
            self.radio_show,
        )  # pragma: no cover

    def natural_key(self):
        return (self.group, self.radio_show, self.permission)
