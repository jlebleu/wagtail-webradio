��    a      $  �   ,      8  P   9     �     �     �     �     �     �     �     �  -   	  0   2	     c	     t	     �	     �	     �	     �	     �	     �	  ]   �	     1
     =
     P
     Y
     ^
     o
     �
     �
     �
     �
  
   �
     �
     �
  (        +     1     6     >     S     j     �     �     �     �  
   �  
   �     �     �     �          (  "   4     W  	   \     f     m     y  A   ~     �  /   �  -     0   :     k     �     �     �  	   �  !   �  v   �  @   a     �  	   �     �  D   �       2   :     m     y     �     �     �     �  
   �     �     �     �     �     �  
               	   !  
   +     6     ;     A  !  S  V   u     �     �     �  "   �           8     M     c  4   y  9   �     �     �            	   !      +     L     a  `   w     �     �     �           	     )     >     R     g     �     �     �  ,   �  2   �                    $     ;     T     o     �     �     �     �  	   �     �     �     �       
   -     8     X     d  
   v     �     �  U   �     �  ;     7   Y  9   �     �     �     �  #        &  (   9  �   b  R         U     v       C   �  ,   �  5   �     5     A     H     Q  !   X  "   z  
   �     �     �     �     �     �  	   �  
   �     �  
                  '     -     $   ;       @   a           R   V   C   P   &      T   Q   
   !   +   9       I   .      (   S   %          )   4      6       L          8   J   "           K       *   _   ]          H         /       U          3   5   -                 #                    >          Y         `      D   Z       O      M       [                 7   1   :       ?   ^   N       X       '   W       A   0       =              E      \           2              	      G             F          ,   <   B    
      There is %(counter)s match
     
      There are %(counter)s matches
     Add Add a podcast Add a radio show Add a radio show permission Add podcast in Add podcasts Add radio show All radio shows Are you sure you want to delete this podcast? Are you sure you want to delete this radio show? Choose a podcast Choose another podcast Contact Date Delete Delete any podcast Delete podcast Delete radio show Deleting '%(escaped_object)s' would require deleting the following protected related objects: Description Download this song Duration Edit Edit any podcast Edit the radio show Edit this podcast Go to the next song Go to the previous song Latest podcasts Loading… Media No podcasts match this filters. Only one sound must be set (file or url) Pause Play Podcast Podcast '{0}' added. Podcast '{0}' deleted. Podcast '{0}' updated. Podcast tag Podcasts Podcasts of Publish date Publishing Radio show Radio show '{0}' added. Radio show '{0}' deleted. Radio show '{0}' updated. Radio show permissions Radio shows Remove this song from the playlist Save Saving… Search Search term Seek The file type {} is not authorized. Authorized file types are {}. The format must be HH:MM:SS The podcast could not be created due to errors. The podcast could not be saved due to errors. The radio show could not be saved due to errors. This slug is already in use. Title Toggle the playback Toggle the playlist display Try again Unable to delete this radio show. Unable to retrieve an audio file. Check that it is valid and supported by your web browser by opening it in a new tab. Unable to retrieve an audio file. Please verify the file format. View podcasts of '%(title)s' Web radio Yes, delete You cannot have multiple permission records for the same radio show. You haven't added any podcasts. You must fill at least a sound file or a sound url description duration email group group radio show permission group radio show permissions permission phone number picture podcast podcasts publish date radio show radio shows slug sound URL sound file tags title {model}: {object} Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-06-28 22:36+0200
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.4.2
 
        Il y a %(counter)s correspondance 
        Il y a %(counter)s correspondances Ajouter Ajouter un podcast Ajouter une émission Ajouter une permission d'émission Ajouter un podcast dans Ajouter des podcasts Ajouter une émission Toutes les émissions Êtes-vous sûr(e) de vouloir supprimer ce podcast ? Êtes-vous sûr(e) de vouloir supprimer cette émission ? Choisir un podcast Choisir un autre podcast Contact Date Supprimer Supprimer n'importe quel podcast Supprimer le podcast Supprimer l'émission Supprimer '%(escaped_object)s' nécessite de supprimer les objets liés et protégés suivants : Description Télécharger ce son Durée Modifier Modifier n'importe quel podcast Modifier l'émission Modifier ce podcast Aller au son suivant Aller au son précédent Derniers podcasts Chargement… Média Auncun podcast ne correspond à ces filtres. Un seul son doit être spécifié (fichier ou url) Mettre en pause Lire Podcast Podcast '{0}' ajouté. Podcast '{0}' supprimé. Podcast '{0}' mis à jour. Étiquette de podcast Podcasts Podcasts de Date de publication Publication Émission Émission '{0} ajoutée. Émission '{0} supprimée. Émission '{0} mise à jour. Permissions d'émission Émissions Supprimer ce son de la playlist Sauvegarder Enregistrement… Rechercher Terme de recherche Naviguer dans le son Le type de fichier {} n'est pas autorisé. Les types de fichiers autorisés sont {}. Le format doit être HH:MM:SS Le podcast ne peut pas être enregistré du fait d'erreurs. Le podcast ne peut être enregistré du fait d'erreurs. L'émission ne peut être enregistrée du fait d'erreurs. Ce slug est déjà utilisé. Titre Basculer la lecture Basculer l'affichage de la playlist Essayer à nouveau Impossible de supprimer cette émission. Impossible de récupérer un fichier audio. Veuillez vérifier qu'il est valide et compatible avec votre navigateur Web en ouvrant l'URL dans un nouvel onglet. Impossible de retrouver un fichier audio. Veuillez vérifier le format de fichier. Voir les podcasts de '%(title)s' Webradio Oui, supprimer Vous ne pouvez avoir plusieurs permissions pour la même émission. Vous n’avez pas encore ajouté de podcast. Vous devez remplir au moins un fichier son ou une URL description durée courriel groupe permission d'émission par groupe permissions d'émission par groupe permission numéro de téléphone image podcast podcasts date de publication émission émissions slug URL du son fichier son étiquettes titre {model} : {object} 